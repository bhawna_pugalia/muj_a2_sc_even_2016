package in.forsk.classwork1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class ScreenB extends AppCompatActivity {
    //TAG for logging
    private final static String TAG = ScreenB.class.getSimpleName();

    //Reference Variable for Xml Views
    private Button finishBtn;
    private TextView randomTv;

    //Reference Variable for random class
    private Random random;

    //Class Variable to store the random int
    private int random_int = -1;

    //Class variable to store the random int string
    private String random_string = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Do not delete or modify these two lines

        //https://docs.oracle.com/javase/tutorial/java/IandI/super.html
        super.onCreate(savedInstanceState);

        //The Activity class takes care of creating a window for you in which you can place your UI with setContentView(View)
        setContentView(R.layout.activity_screen_b);

        //Start writing your code from here.


        /*
        //Get the Random number generated in screen A

        //getIntent() :-  This method will return the object of the intent through this activity is being called
        //hasExtra(<Key>) :- This method will check if the current intent bundle have the key-value pair
        if(getIntent().hasExtra(ScreenA.RANDOM_STRING)){
            //getStringExtra() :- this method will return the value stored in the key.
            random_string = getIntent().getStringExtra(ScreenA.RANDOM_STRING);
        }
        */

        //init random variable
        //always check for null before creating object.
        if (random == null) {
            random = new Random();
        } else {
            Log.d(TAG, "Random Object already in memory !!");
        }
        if (random == null) {
            random = new Random();
        } else {
            Log.d(TAG, "Random Object already in memory !!");
        }

        //point the reference variable to the repective object of views in XML
        finishBtn = (Button) findViewById(R.id.finishBtn);
        randomTv = (TextView) findViewById(R.id.randomTv);

        //Setting label to button and text view
        finishBtn.setText("Finish");
        randomTv.setText("Random int");

        //Generating Random int
        random_int = random.nextInt();

        //Convert int to string
        random_string = String.valueOf(random_int);

        //Setting random text to textVie
        randomTv.setText(random_string);

        //Setting on Click listener using anonymous class concept
        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
