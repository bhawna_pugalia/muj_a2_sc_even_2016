package log.forsk.in.logandbreakpoints;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


// Dr. Sylvester Fernandes
// Saurabh K. Sharma

// Commented by Dr. Sylvester Fernandes on 10th Feb 2016 at 7 30 PM

// Commented by Dr. Sylvester Fernandes on 10th Feb 2016 at 7 35 PM

// Commented by Saurabh K. Sharma on 10th Feb 2016 at 07.36 PM

// Commented by Dr. Sylvester Fernandes on 10th Feb 2016 at 7 39 PM


//Interface OnClickListener
//Use Ctrl+Click to see the source code of the interface
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String TAG = MainActivity.class.getSimpleName();
    private Context context;
    //Use camel case
    Button loggingBtn, breakPointBtn, toastBtn, genericBtn, eventHandlingBtn, wrapperBtn,inhertanceBtn,polymorphismBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        //Type casting in button object  button_view = (Button) view ;
        loggingBtn = (Button) findViewById(R.id.button);
        breakPointBtn = (Button) findViewById(R.id.button2);
        toastBtn = (Button) findViewById(R.id.button3);
        genericBtn = (Button) findViewById(R.id.button4);
        eventHandlingBtn = (Button) findViewById(R.id.button5);
        wrapperBtn = (Button) findViewById(R.id.button6);
        inhertanceBtn = (Button) findViewById(R.id.button7);
        polymorphismBtn = (Button) findViewById(R.id.button8);


        loggingBtn.setOnClickListener(this);
        breakPointBtn.setOnClickListener(this);
        toastBtn.setOnClickListener(this);
        genericBtn.setOnClickListener(this);
        wrapperBtn.setOnClickListener(this);
        inhertanceBtn.setOnClickListener(this);
        polymorphismBtn.setOnClickListener(this);


        eventHandlingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Event Handling");
            }
        });


    }

    //File > setting > keymap > eclipse
    //Ctrl+A to select all and then Ctrl+f to auto format the code

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.button:
                Log.d(TAG, "Debug Log");
                Log.v(TAG, "Verbose Log");
                Log.e(TAG, "Error Log");
                Log.i(TAG, "Info Log");
                Log.w(TAG, "Warning Log");
                break;
            case R.id.button2:

                for (int i = 0; i < 9; i++) {
                    Log.d(TAG, "Value for i : " + i);
                    Log.d(TAG, "Put your breakpoint here");
                }

                break;
            case R.id.button3:

                Toast.makeText(context, "Toast !!", Toast.LENGTH_LONG).show();

                break;
            case R.id.button4:

                printGeneric();

                break;
            case R.id.button6:

                MyModel myModel = new MyModel();

                List<MyModel.Person> personArrayList = myModel.getPersons();

                for(MyModel.Person person : personArrayList){
                    Log.d(TAG,person.toString());
                }

                break;
            case R.id.button7:

                int a = 20, b = 10;
                My_Calculation demo = new My_Calculation();
                demo.addition(a, b);
                demo.Substraction(a, b);
                demo.multiplication(a, b);

                break;
            case R.id.button8:

                Salary s = new Salary("Yogendra Sharma", "jaipur , RAJ", 3, 3600.00);
                Employee e = new Salary("Rohit Mishra", "jaipur , RAJ", 2, 2400.00);
                System.out.println("Call mailCheck using Salary reference --");
                s.mailCheck();
                System.out.println("\n Call mailCheck using Employee reference--");
                e.mailCheck();

                break;
        }

    }

    public void printGeneric() {
        // Create arrays of Integer, Double and Character
        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4};
        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};

        System.out.println("Array integerArray contains:");
        GenericMethod.printArray(intArray); // pass an Integer array

        System.out.println("\nArray doubleArray contains:");
        GenericMethod.printArray(doubleArray); // pass a Double array

        System.out.println("\nArray characterArray contains:");
        GenericMethod.printArray(charArray); // pass a Character array

        ArrayList<String> arrayListString = new ArrayList<String>();
        arrayListString.add("Value 1");
        arrayListString.add("Value 2");
        arrayListString.add("Value 3");
        arrayListString.add("Value 4");
        arrayListString.add("Value 5");

        for (String s : arrayListString) {
            Log.d(TAG, s);
        }


    }
}
